This repository contains the results of Propulsion Academy's required pre-work.

The respository is divided in 3 folders:

1. htmlcss  
This folder contanis the 2 exercises of the HTML and CSS part of the materials (Google and Instagram clones).

2. javascript  
This folder contains the 15 basic exercises (1.js - 15.js), as well as the 3 javascript projects (bank, music player and calculator), all with the respective bonuses.

3. python  
This folder contains the 15 python exercises corresponding to the backend part of the pre-work, as well as the music player project.
