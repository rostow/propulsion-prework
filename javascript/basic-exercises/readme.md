# Introduction

The included javascript files are the results of the exercises corresponding to the Javascript part of Propulsion Academy's required pre-work.

The exercises included are:

1. Reverse
2. Factorial
3. Longest word
4. Sum nums
5. Time conversion
6. Count vowels
7. Palindrome
8. Most letters
9. Two sum
10. Is power of Two
11. Repeat a string
12. Sum all numbers in a Range
13. True or False
14. Return largest numbers in arrays
15. Is it an anagram?


## Instructions to run the exercises

In order to run the excersises you will need a version of NodeJS installed in your computer.

From the terminal, run:

```console
foo@bar:~$ node [exercise number].js
```

where exercise must be the number of the exercise.

The console will output the results of the tests suggested in the exercises description, e.g.:

```shell
foo@bar:~$ node 1.js
ymedacA noisluporP
olleH
dcba
```


