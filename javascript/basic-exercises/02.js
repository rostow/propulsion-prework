// 2. Factorial
// =============
// Write a method that takes an integer n in; 
// it should return n*(n-1)*(n-2)*...*2*1. Assume n >= 0.
// As a special case, factorial(0) == 1.

function factorial(n) {
    if (n < 0) {
        return "n must be > or = to 0";
    }
    else if (n === 0){
        return 1;
    }
    else if (n > 0) {
        output = 1;
        while (n>0){
            output *= n;
            n = n-1;
        }
        return output;
    }
    else {
        return "n must be a number";
    }
}


// tests
console.log(factorial(5)) // 120
console.log(factorial(4)) // 24
console.log(factorial(0)) // 1
console.log(factorial(-1)) // "n must be > or = to 0"