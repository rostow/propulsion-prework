// 11. Repeat a string
// ====================
// Repeat a given string (first argument) num times (second argument). 
// Return an empty string if num is not a positive number.

function repeat_string_num_times(str, num) {
    if (num<=0) {
        return '';
    } else {
        return str.repeat(num);
    }
}


// tests
console.log(repeat_string_num_times("abc", 3)) // 'abcabcabc'
console.log(repeat_string_num_times("abc", 1)) // 'abc'
console.log(repeat_string_num_times("abc", 0)) // ''
console.log(repeat_string_num_times("abc", -1)) // ''