// 3. Longest Word
// ===============
// Write a method that takes in a string. Return the longest word in the string. 
// You may assume that the string contains only letters and spaces. 
// You may use the String split method to aid you in your quest.

function longest_word(sentence) {
    var lst = sentence.split(" ");
    var longest = lst[0]
    for (var i=1; i<lst.length; i++) {
        if (lst[i].length > longest.length) {
            longest = lst[i];
        }
    }
    return longest;
}


// tests
console.log(longest_word("This is an amazing test")) // "amazing"
console.log(longest_word("Laurent Colin")) // "Laurent"
console.log(longest_word("Propulsion 123")) // "Propulsion"