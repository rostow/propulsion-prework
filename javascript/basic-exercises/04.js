// 4. Sum Nums
// ============
// Write a method that takes in an integer num and returns 
// the sum of all integers between zero and num, up to and including num.

function sum_nums(num) {
    if (!Number.isInteger(num)) {
        return "num is not a valid integer!";
    };
    var output = 0;
    while (num > 0 || num < 0) {
        output += num;
        if (num < 0) {
            num += 1;
        } else {
            num -= 1;
        }
    }
    return output;
}


// tests
console.log(sum_nums(6)) // 21
console.log(sum_nums(1)) // 1
console.log(sum_nums(0)) // 0
console.log(sum_nums(-6)) // -21
console.log(sum_nums('hola')) // num is not a valid integer!