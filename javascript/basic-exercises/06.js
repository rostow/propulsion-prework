// 6. Count Vowels
// ===============
// Write a method that takes a string and returns the number of vowels in the string. 
// You may assume that all the letters are lower cased. You can treat “y” as a consonant.

function count_vowels(string) {
    var vcount = 0;
    for (var i = 0; i< string.length; i++) {
        if ('aeiou'.match(string[i].toLowerCase())) {
            vcount+=1;
        }
    }
    return vcount;
}


//tests
console.log(count_vowels("alphabet")) // 3
console.log(count_vowels("Propulsion Academy")) // 7
console.log(count_vowels("AaaAa")) // 5
console.log(count_vowels("fly")) // 0