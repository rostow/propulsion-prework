// 12. Sum All Numbers in a Range
// ==============================
// Write a function that receives an array of two numbers as argument 
// and returns the sum of those two numbers and all numbers between them.

function add_all(arr) {
    var output = 0;
    var num = arr[1];
    while (num >= arr[0]) {
        output += num;
        num -= 1;
    }
    return output;
}


// tests
console.log(add_all([1, 4])) // 10
console.log(add_all([5, 10])) // 45
console.log(add_all([9, 10])) // 19
console.log(add_all([0, 0])) // 0
console.log(add_all([-1, 1])) // 0