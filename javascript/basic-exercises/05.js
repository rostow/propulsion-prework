// 5. Time Conversion
// ===================
// Write a method that will take in a number of minutes, 
// and returns a string that formats the number into hours:minutes.

function time_conversion(minutes) {
    var hours = ('0' + ((minutes - (minutes % 60)) / 60)).slice(-2);
    var minutes = ('0' + (minutes % 60)).slice(-2);
    return `${hours.padStart(2,'0')}:${minutes.padStart(2,'0')}`;
}


// tests
console.log(time_conversion(155)) // "02:35"
console.log(time_conversion(61)) // "01:01"
console.log(time_conversion(60)) // "01:00"
console.log(time_conversion(59)) // "00:59"