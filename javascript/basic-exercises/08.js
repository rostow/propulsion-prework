// 8. Most Letters
// ===============
// Write a method that takes a string in and returns true if 
// the letter “z” appears within three letters after an “a”. 
// You may assume that the string contains only lowercase letters.

function nearby_az(string) {
    return (string.indexOf('z') >= 0) && (string.indexOf('z') - string.indexOf('a')) <= 3;
}


// tests
console.log(nearby_az("abbbz")) // false
console.log(nearby_az("abz")) // true
console.log(nearby_az("abcz")) // true
console.log(nearby_az("abba")) // false