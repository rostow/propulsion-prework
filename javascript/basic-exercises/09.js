// 9. Two Sum
// ===========
// Write a method that takes an array of numbers. If a pair of numbers 
// in the array sums to zero, return the positions of those two numbers. 
// If no pair of numbers sums to zero, return null.

function two_sum(nums) {
    var output = [];
    for (var i = 0; i< nums.length; i++) {
        for (var j = 0; j< nums.length; j++) {
            if (nums[i] + nums[j] === 0) {
                output.push([i,j]);
            }
        }
    }
    if (output.length > 0) {
        return output.slice(0,(output.length/2));
    } else {
        return null;
    }
}


// tests
console.log(two_sum([1, 3, -1, 5])) // [[0, 2]]
console.log(two_sum([1, 3, -1, 5, -3])) // [[0, 2], [1, 4]]
console.log(two_sum([1, 5, 3, -4])) // null