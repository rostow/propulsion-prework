// 1. Reverse
// ==============
// Write a method that will take a string as input, 
// and return a new string with the same letters in reverse order.
////////////////////////////


function reverse(string) {
    return string.split("").reverse().join("");
}


// tests
console.log(reverse("Propulsion Academy")); // "ymedacA noisluporP"
console.log(reverse("Hello")) // "olleH"
console.log(reverse("abcd")) // "dcba"