//////////////////////////
// MUSIC PLAYER PROJECT //
/////////////////////////


// Player class
function Player () {
    this.tracks = [];
    this.currentTrack = 0;
    this.stopped = true;
}

Player.prototype.add = function (track) {
    this.tracks.push(track);
}

Player.prototype.play = function (track=undefined) {

    this.stopped = false;

    if (track) {
        console.log('custom track playing!');
        this.currentTrack = this.tracks.indexOf(track);
    }

    // we need to declare a local variable equal to "this" or else 
    // "this" won't refer to the player instance inside the setInterval function
    var player = this;

    playLoop = setInterval(function () {

        var currentTrack = player.tracks[player.currentTrack];

        if (player.currentTrack === player.tracks.length-1 || player.stopped) {
            player.stopped = true;
            // breaks out of the loop when the player reaches the final track
            clearInterval(playLoop);
        }

        currentTrack.play();
        player.next();

    }, 2000);
}

/**
 * @Description Stops the player. 
 * @Parameters Accepts optional argument "delay" (in seconds). Default is 0 (no delay). 
 */
Player.prototype.stop = function (delay=0) {

    var player = this;

    if (!player.stopped) {
        setTimeout(function () {
            clearInterval(playLoop);
            player.stopped = true;
            console.log('\nPlaylist has been stopped.');
        }, delay*1000);
    }
}

Player.prototype.next = function() {
    if (this.currentTrack + 1 <= this.tracks.length - 1) {
        this.currentTrack +=1;
    } else {
        this.currentTrack = 0;
    }
}

Player.prototype.previous = function () {
    if (this.currentTrack - 1 >= 0) {
        this.currentTrack -= 1;
    } else {
        this.currentTrack = this.tracks.length - 1;
    }
}

Player.prototype.printAllTracks = function () {
    console.log('TRACK LIST');
    console.log('==========');
    for (var i=0; i<this.tracks.length;i++) {
        var track = this.tracks[i];
        console.log(`Title: ${track.title} | Artist: ${track.artist} | Album: ${track.album}`);
    }
    console.log('\n');
}

// Track class
function Track (artist, title, album) {
    this.artist = artist;
    this.title = title;
    this.album = album;
}

Track.prototype.play = function () {
    console.log("Playing: " + this.title + " by " + this.artist);
}

///////////////////////////////////////////
///////////////// TESTS ///////////////////
//////////////////////////////////////////

// declare instances
var player = new Player ();
var driveTrack = new Track('Incubus', 'Drive', 'Make Yourself');
var laBambaTrack = new Track('Ritchie Valens', 'La Bamba', 'La Bamba');
var huidaTrack = new Track('Ismael Serrano', 'La huida', 'Paraisos Aparentes');
var withinTrack = new Track('Michel Camilo', 'From Within', 'Calle 54');
var silverTrack = new Track('Pete Murray', 'Silver Cloud', 'Summer at Eureka');

// add tracks to player
player.add(driveTrack);
player.add(laBambaTrack);
player.add(huidaTrack);
player.add(withinTrack);
player.add(silverTrack);

// print all tracks in player
player.printAllTracks();


// IMPORTANT: 
// the next 3 tests need to be run separately (asynchronous issues), 
// so please run only one test at a time and comment the other two.

// test1: full playlist stopping at the last track automatically
player.play();

// test2: play specific track and continue playing until the last track
// player.play(withinTrack);

// test3: play tracks and stop after 8 seconds
// player.play();
// player.stop(8);