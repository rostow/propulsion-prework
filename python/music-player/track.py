# Track class
     
class Track(object):
    
    def __init__(self, artist, title, album):
        self.artist = artist
        self.title = title
        self.album = album
    
    def play(self):
        print("Playing: {0} ({1}) by {2}".format(self.title,self.album,self.artist))

