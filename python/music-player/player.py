# Player class

class Player(object):
    
    def __init__(self,name):
        self.name = name
        self.tracks = []
        self.currentTrack = 0

    def add(self, track):
        self.tracks.append(track)

    def play(self, track=None):
        if track:
            self.currentTrack = self.tracks.index(track)
        
        self.tracks[self.currentTrack].play()

    def next(self):
        if self.currentTrack + 1 <= len(self.tracks) - 1:
            self.currentTrack += 1
        else:
            self.currentTrack = 0
    
    def previous(self):
        if self.currentTrack - 1 >= 0:
            self.currentTrack -= 1
        else:
            self.currentTrack = len(self.tracks) - 1

    def printTracksInfo(self):
        for t in self.tracks:
            print("Track {0}: {1} ({2}) by {3}"\
                .format(self.tracks.index(t),t.title,t.album,t.artist))