# 1. Command Line Arguments
# Instead of printing “Hello, World!” to the console, we would like to say 
# hello to a person whose name is supplied as a command line argument to our application.
# If multiple command line arguments are supplied to our Hello application, say hello to each of the named persons. 

import sys

def hello_world(args):
    if len(args) < 2:
        print("Hello, Unkwon!")
    for a in args[1:]:
        print("Hello, " + a + "!")

hello_world(sys.argv)