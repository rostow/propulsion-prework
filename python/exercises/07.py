# 7. Find the second smallest element in an array
# Write a function to find the second smallest element in an array:

def find_second_smallest(arr):
    
    return sorted(arr)[1]

print(find_second_smallest([0, 3, -2, 4, 3, 2]))  #0
print(find_second_smallest([10, 22, 10, 20, 11, 22]))  #10