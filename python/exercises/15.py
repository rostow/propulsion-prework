# 15. Find the Digit
# Write a function that takes a positive number as a parameter (n) and returns 
# the number of times you must multiply its digits until you reach a single digit.
# For example: find(57) == 3, because 5 * 7 = 35, 3 * 5 = 15, and 1 * 5 = 5.

from functools import reduce

def find(number, count=0):
    while len(str(number)) > 1:
        count+=1
        number = reduce(lambda x,y: int(x)*int(y), list(str(number)))
    return count

print(find(57)) # 3
print(find(5923)) # 2
print(find(90)) # 1
print(find(7)) # 0
print(find(999)) # 4