# 5. Vowels in a string
# Write a function to count all vowels in a string.

def count_of_vowels(args):
    count = 0
    for l in args.lower():
        if l in 'aeiou':
            count += 1
    return count

print(count_of_vowels("Propulsion Academy"))