# 6. Check valid password
# Write a function that checks the user’s input and validate it based on the notes below.
# -A password must have at least ten characters.
# -A password consists of only letters and digits.
# -A password must contain at least a capital letter in it.

import string

def is_valid_password(inp):
    capital = False
    if len(inp) < 10:
        print('Invalid password: must have at least ten characters.')
        return False
    for c in inp:
        if c in string.punctuation:
            print('Invalid password: should contain only letters and digits.')
            return False
        if c in string.ascii_uppercase:
            capital = True
    if not capital:
        print('Invalid password: must contain at least a capital letter.')
        return False
    print('Password is valid!')
    return True

inp = input('Enter your password: ')
is_valid_password(inp)