# 13. Prime Numbers
# Write a function that prints out all prime numbers up until n using an iterative loop.

def is_prime(n):
    if n < 2:
        return False
    if n == 2:
        return True
    for i in range(2, n):
        if n%i==0:
            return False
    return True

def prime_number(n):
    primes = []
    for i in range(1,n+1):
        if is_prime(i):
            primes.append(i)
    return primes
        

print(prime_number(20))