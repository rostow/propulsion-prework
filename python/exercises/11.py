# 11. Fibonacci
# Write a function that computes the nth Fibonacci number, 
# where f(n) = f(n-1) + f(n-2), given n >= 0, f(0) = 0, and f(1) = 1.

import sys

def fibonacci(num):
    if num == 0 or num == 1:
        return num
    return fibonacci(num-1)+fibonacci(num-2)


print(fibonacci(int(sys.argv[1])))