# 4. Middle character of a string
# Write a function to display the middle character of a string.

import math

def get_middle_character(string):
    
    if len(string) % 2 == 0:
        
        start = math.floor((len(string)/2)-1)
        end = math.floor(len(string)/2)+1
        
        return string[start:end]
    
    return string[math.floor(len(string)/2.0)]

print(get_middle_character("3500"))
print(get_middle_character("35790"))
print(get_middle_character("12drgw3500"))