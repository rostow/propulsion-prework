# 10. Length of the longest sequence of zeros in binary representation of an integer
# Write a function to find the length of the longest sequence of zeros in binary representation of an integer.

def get_size_of_longest_sequence_of_zeros(num):
    lst = str(bin(num)).split("1")
    return len(sorted(lst)[len(lst)-2])
    

print(get_size_of_longest_sequence_of_zeros(7)) # binary representation: 111 - 0
print(get_size_of_longest_sequence_of_zeros(8)) # binary representation: 1000 - 3
print(get_size_of_longest_sequence_of_zeros(457)) # binary representation: 111001001 - longest 2
print(get_size_of_longest_sequence_of_zeros(40)) # binary representation: 101000 - longest 3
print(get_size_of_longest_sequence_of_zeros(12546)) # binary representation: 11000100000010 - longest 6