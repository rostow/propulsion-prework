# 8. Remove duplicate elements from an array
# Write a function that removes duplicate entries in an array:

def unique_array(arr):
    return list(dict.fromkeys(arr))

print(unique_array([0, 3, -2, 4, 3, 2]))   # [0, 3, -2, 4, 2]
print(unique_array([10, 22, 10, 20, 11, 22]))  #[10, 22, 20, 11]