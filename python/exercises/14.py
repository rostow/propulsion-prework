# 14. Sorting an Array
# Given an array of numbers, sort them in such a manner that all the odd numbers in the array 
# are sorted in ascending order and the even numbers are sorted in descending order after the 
# last odd number. For example [1,2,3,4,5,6,7,8,9] produces the output [1,3,5,7,9,8,6,4,2]. 
# If the array contains decimals, round them down while checking for odd/even.

from math import floor 

def sort_it(arr):
    odd_arr, even_arr = [], []
    for n in arr:
        if floor(n)%2 == 0:
            even_arr.append(n)
        else:
            odd_arr.append(n)
    return sorted(odd_arr)+sorted(even_arr, reverse=True)
    
    
print(sort_it([1, 2, 3, 4, 5, 6, 7, 8, 9]))
print(sort_it([26.66, 24.01, 52.00, 2.10, 44.15, 1.02, 11.15]))