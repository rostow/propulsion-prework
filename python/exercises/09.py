# 9. Pentagonal numbers
# Write a function to display the first 50 pentagonal numbers.

import math
def get_pentagonal_number(n):
    for i in range(1,n+1):
        print(math.floor((3*i*i-i)/2),end = '\t')

get_pentagonal_number(50)