# 2. List of all files and directories names
# Write a function to print a list of all file/directory names from the working directory.
# Write a function to print a list of all file/directory names from the given path pass as an argument.

import os, sys

def files_and_dirlist(arg):
    if len(arg) > 1:
        return os.listdir(arg[1])
    else:
        return os.listdir()

print(files_and_dirlist(sys.argv))