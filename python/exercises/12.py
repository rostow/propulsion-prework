# 12. Factorial
# Write a function that computes the factorial of a non-negative 
# integer (denoted n!), where f(n) = n * f(n-1), given n >= 0 and f(0) = 1.
# Write both recursive and iterative functions.

def factorial(n):
    if n == 0:
        return 1
    return n * factorial(n-1)

def factorial_iterative(n):
    output = 1
    if n == 0:
        return 1
    while n > 0:
        output *= n
        n = n-1
    return output

print(factorial(5))
print(factorial_iterative(5))