# 3. Arrays are equal
# Write a function to test if two arrays contain the same integers

import unittest

arr1 = [2, 5, 7, 9, 11]
arr2 = [2, 5, 7, 8, 11]
arr3 = [2, 5, 11, 9, 7]

def are_two_arrays_equal(arg1, arg2):
    return sorted(arg1) == sorted(arg2)


print(are_two_arrays_equal(arr1, arr2))  # false
print(are_two_arrays_equal(arr1, arr3))  # true